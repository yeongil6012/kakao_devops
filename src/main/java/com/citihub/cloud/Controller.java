package com.citihub.cloud;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;


@RestController
public class Controller {

   /**
    * Get IP of the System.
    *
    * @return IP of the System
    */
   @GetMapping("/")
   public static String getIP() {
       InetAddress ip = null;
       try {
       ip = InetAddress.getLocalHost();
       } catch (UnknownHostException e) {
       e.printStackTrace();
       }
       return "hostname:" + ip.getHostName() + "@@@"+"host ip:" + ip.getHostAddress();
   }
}

