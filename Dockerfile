# # docker push yeongil/kakao_devops:0.0.1-snapshot
# #
# # Extends ubuntu-base with java 8 openjdk jdk installation
# #
# FROM ubuntu:18.04

# # Set default shell to /bin/bash
# SHELL ["/bin/bash", "-cu"]

# ARG DEBIAN_FRONTEND=noninteractive

# RUN apt-get update && apt-get install -y openjdk-8-jdk \
# 	curl \
#     vim \
#     wget \
# 	git

# COPY build/libs/aks-accelerator.jar app.jar
# ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
# RUN export JAVA_HOME

# EXPOSE 8080
# WORKDIR "/usr/local"
# CMD ["java","-jar","/app.jar"]


ARG PROJECT_DIR
FROM openjdk:8-jre-alpine
EXPOSE 8080/tcp
ADD $PROJECT_DIR/build/libs/aks-accelerator.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
