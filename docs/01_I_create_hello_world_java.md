# 01 I - Writing a “Hello World” Application in Spring Boot
_Aim: To create a “Hello World” application on a local machine (using Java) and put it in a Docker container._ 


## 1.0 Introduction
We can create _Hello World_ in any language we choose, but because: 
- it remains popular in the Enterprise 
- it is well suited to microservices 

we will choose here to use [Spring Boot](https://spring.io/projects/spring-boot) (Java). 

> Note: if you're reading a PDF copy of this document, the source code is available on Gitlab at [https://gitlab.com/citihub/aks-accelerator](https://gitlab.com/citihub/aks-accelerator). It can be cloned, using Git Bash or PowerShell, as follows:

```
➜  git clone git@gitlab.com:citihub/aks-accelerator.git
```
> Note: if you're new to Git you will first need to setup an SSH keypair so you can authenticate. See [GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/README.html) for instructions.

## 1.1 Writing Hello World 

> Note: we include this section not as a programming course, but to illustrate the process of authoring, containerising, building and running an application on Kubernetes from end-to-end.

Spring Boot favours convention over configuration, and so we need very little boilerplate code in order to get started.

We can write two simple classes as follows:

### 1.1.1 Application.java

The class that will use to launch our application.

```
package com.citihub.cloud; 
 
import org.springframework.boot.SpringApplication; 
import org.springframework.boot.autoconfigure.SpringBootApplication; 
 
@SpringBootApplication 
public class Application { 
 
   public static void main(String[] args) { 
       SpringApplication app = new SpringApplication(Application.class); 
       app.run(); 
   } 
} 
``` 

### 1.1.2 Controller.java

The class that we will use to respond to HTTP GET requests with a "hello world" message.

```
package com.citihub.cloud; 
 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.RestController; 
 
@RestController 
public class Controller { 
 
   @GetMapping(path="/") 
   public String getHome() { 
       return "Hello world. Application demonstrates Gitlab, K8s integration."; 
   } 
} 
```

### 1.1.3 Dependency & Build Management
To build our code and manage our dependencies, we choose here to use [Gradle](https://gradle.org).

> Note: we have no particular requirement to use Gradle. [Maven](https://maven.apache.org), a similar tool, would work equally well.

In our `build.gradle` file, we declare the plugins we want to use and the dependencies we need in order to compile our code.

We will containerise the application along with all of its dependencies using Docker later.

```
buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.1.3.RELEASE")
    }
}

apply plugin: 'java'
apply plugin: 'idea'
apply plugin: 'org.springframework.boot'
apply plugin: 'io.spring.dependency-management'

repositories {
    mavenCentral()
}

sourceCompatibility = 1.8
targetCompatibility = 1.8

dependencies {
    compile("org.springframework.boot:spring-boot-starter-web")
}
```

## 1.2 Running our Application
We now have everything we need to build and run a simple HTTP microservice. In a Terminal, in the root of our project, we can do the following:

```
➜  ./gradlew bootRun
```

We should see the application launch, log some output, and the following message:


```
Tomcat started on port(s): 8080 (http) with context path ''
Started Application in 2.183 seconds (JVM running for 2.547)
```

## 1.3 Using our Application
With our application running, we can now test it with our browser or with the command line:

```
➜  ~ curl -X GET http://127.0.0.1:8080
Just hello world. Application demonstrates Gitlab, K8s integration.
```

> Note: `curl` is an extremely useful and widely used command line tool for communicating with HTTP endpoints. It's worth familiarising yourself with the various options.

## Next
In the next section, we will containerise our simple application.

< [00 Prerequisites](00_prerequisites.md) | 01 Writing a “Hello World” Application | [02 Containerising a “Hello World” Application](02_containerising.md) >