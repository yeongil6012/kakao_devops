#!/usr/bin/env bash

# Prepare - will need in order to get azure-cli
##
AZ_REPO=$(lsb_release -cs)
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
    sudo tee /etc/apt/sources.list.d/azure-cli.list

apt-key --keyring /etc/apt/trusted.gpg.d/Microsoft.gpg adv \
     --keyserver packages.microsoft.com \
     --recv-keys BC528686B50D79E339D3721CEB3E94ADBE1229CF


apt-get update
apt-get install -y git

# Azure CLI dependencies (https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-apt?view=azure-cli-latest)
##
apt-get install apt-transport-https lsb-release software-properties-common dirmngr -y
apt-get install azure-cli -y

apt-get install snap -y
snap install kubectl --classic
snap install terraform

# We get a complaint about locale on first `vagrant ssh`
##
locale-gen en_GB.UTF-8
